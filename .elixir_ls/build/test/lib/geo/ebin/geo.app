{application,geo,
             [{applications,[kernel,stdlib,elixir,logger]},
              {description,"Encodes and decodes WKB, WKT, and GeoJSON formats.\n"},
              {modules,['Elixir.Geo','Elixir.Geo.GeometryCollection',
                        'Elixir.Geo.JSON','Elixir.Geo.JSON.Decoder',
                        'Elixir.Geo.JSON.Decoder.DecodeError',
                        'Elixir.Geo.JSON.Encoder',
                        'Elixir.Geo.JSON.Encoder.EncodeError',
                        'Elixir.Geo.LineString','Elixir.Geo.MultiLineString',
                        'Elixir.Geo.MultiPoint','Elixir.Geo.MultiPolygon',
                        'Elixir.Geo.Point','Elixir.Geo.PointM',
                        'Elixir.Geo.PointZ','Elixir.Geo.PointZM',
                        'Elixir.Geo.Polygon','Elixir.Geo.Utils',
                        'Elixir.Geo.WKB','Elixir.Geo.WKB.Decoder',
                        'Elixir.Geo.WKB.Encoder','Elixir.Geo.WKB.Reader',
                        'Elixir.Geo.WKB.Writer','Elixir.Geo.WKT',
                        'Elixir.Geo.WKT.Decoder','Elixir.Geo.WKT.Encoder',
                        'Elixir.String.Chars.Geo.GeometryCollection',
                        'Elixir.String.Chars.Geo.LineString',
                        'Elixir.String.Chars.Geo.MultiLineString',
                        'Elixir.String.Chars.Geo.MultiPoint',
                        'Elixir.String.Chars.Geo.MultiPolygon',
                        'Elixir.String.Chars.Geo.Point',
                        'Elixir.String.Chars.Geo.PointM',
                        'Elixir.String.Chars.Geo.PointZ',
                        'Elixir.String.Chars.Geo.PointZM',
                        'Elixir.String.Chars.Geo.Polygon']},
              {registered,[]},
              {vsn,"3.1.0"}]}.
